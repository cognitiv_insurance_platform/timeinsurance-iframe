import Head from "next/head";
import Image from "next/image";
import styles from "../styles/success.module.css";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import Loader from "../components/loader/loader";
import Link from "next/link";
export default function Home() {
  const router = useRouter();

  return (
    <div className={styles.formCard}>
      <h1 className={styles.success}>Your broker will be in touch</h1>
      <p className={styles.successP}>
        Please verify your account by clicking on the link sent to your email.
      </p>
      <button className={styles.button}>
        <a href="https://www.folio.insure" target="_blank">
          Sign in to Folio
        </a>
      </button>
    </div>
  );
}
